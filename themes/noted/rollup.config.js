import resolve from "@rollup/plugin-node-resolve";
import {terser} from "rollup-plugin-terser";

export default {
    input: 'src/main.js',
    output: {
        dir: 'static',
        format: 'iife',
    },
    plugins: [
        resolve(),
        terser()
    ],
    context: "window"
};