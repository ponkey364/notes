module.exports = {
  purge: ["layouts/**/*.html"],
  experimental: {
    darkModeVariant: process.env.DARK_GEN,
  },
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
    },
    extend: {
      fontFamily: {
        display: ["Josefin Sans"]
      }
    }
  },
  variants: {},
  plugins: [
      require("@tailwindcss/typography"),
  ],
}
