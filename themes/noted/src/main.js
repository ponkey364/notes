import "katex/dist/katex";
import autoRender from "katex/contrib/auto-render/auto-render"

import macros from "./texmacros";

autoRender(document.body, {
    macros
})