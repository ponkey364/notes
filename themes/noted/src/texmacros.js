export default {
    '\\diffr': '\\frac {d#1} {d#2}',
    '\\on': '\\operatorname{#1}'
}